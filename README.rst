
Original Authors
================

This code was originally published by Timo Maier and Tamas Haraszti with their paper
`"Python algorithms in particle tracking microrheology" <https://dx.doi.org/10.1186%2F1752-153X-6-144>`_
in the Chemistry Central Journal.

I copied their code and made slight changes to make it run with current version of Python and its popular
libraries. They published the code under LGPLv3 license.

The original README was moved to file ORIGINAL_README.txt.

README
======

.. contents:: README Contents
   :local:

This is a small python package to do rheology related calculations in passive
particle tracking microrheology.

Requirements
------------

It may also work with some earlier versions, but these have been used for
testing:

* python 3.8
* numpy 1.18
* scipy 1.4
* matplotlib 3.2
* pandas 1.0


Capabilities
------------

* calculating mean squared displacement from positions
* higher order displacements
* maximum mean excursion (quadratic and higher as well)
* creep-compliance from MSD
* complex shear modulus from creep compliance using various methods

 The last one can be done using the method proposed by Mason et al. and
 Dasgupta et al., and the numerical transform proposed by Evan et al. There is
 an improved method as well, using local fitting and then a dynamically
 refined data set to get the complex shear modulus.

Mason's method is limited to power functions, so be careful using it. The
Evans method is very sensitive to data noise and to sampling error at high
frequenxies (producing a cut off).

An example script  and general tool is provided in the Examples under the
ProcessRheology.py. It uses a config file to receive commands, there is a
commented example in the same folder.

The full description of the configuration is in the config.txt.example file,
and an example dataset and correscponding config file is also provided in the
Examples folder (fov2191wDD.txt, timestamp-table.txt, config.txt). The example
data is an actin network on micropillars bundled using a testbuffer containing
8mM magnesium (from 25th July 2012, Timo Maier).

The Function-test.py runs some theoretical tests and shows the results
graphically. It is a useful tool to test the functions and see how the various
results change when one varies the parameters (see the comments within).


Install
-------

Download
........

You can manually donwload the code, extract it, navigate to the folder,
and install it with

.. code::

   $ pip install .

Directly from gitlab repository
...............................

Python's package manager pip can also handle installing directly from the git repository.
This requires git to be installed.
In a terminal, enter the command:

.. code::

   $ pip install git+https://gitlab.gwdg.de/janshoff_lab/rheology


Usage
-----

The common import utility will work (``from Rheology import *``).
