#!/usr/bin/env python
from distutils.core import setup
PkgName = "Rheology"
setup(name='rheology',
      version='1.0.0',
      description='Routines to calculate microrheology from particle tracks',
      author=['Tomio', "Ilyas Kuhlemann"],
      author_email='ilyasp.ku@gmail.com',
      url='https://gitlab.gwdg.de/janshoff_lab/rheology',
      license="LGPLv3",
      package_dir={PkgName: "Rheology"},
      packages=[PkgName],
      install_requires=['numpy', 'scipy', 'matplotlib', 'pandas'],)
