from typing import Dict, List, Tuple
from time import ctime
from pickle import load, dump
from configparser import ConfigParser
import os.path
import numpy as np
from matplotlib import pyplot as plt
from .Rheology import (MSD, MKD, MSD_power_fit, MSD_interpolate,
                       MSD_to_J, J_to_G, J_to_G_Mason)

################# Default parameters:
scalermodes = ['scaler', 'interval', 'delayed', 'inherit', 'inheritscaler']

#Some helper functions for processing:
def read_conf(filename: str, verbose: bool=False) -> ConfigParser:
    """ 
    Parse config file or return default config
    """

    if not os.path.isfile(filename):        
        defaultconf = ConfigParser(allow_no_value=True )
        defaultconf['DEFAULT'] = {'verbose': 0}
        defaultconf['PATHS'] = {"dir": "./", "outdir": "./"}
        defaultconf['FIT POWER LAW'] = {"scalermode": "scaler" }
        defaultconf['PLOTS'] = {'dpi': 150, 'ext': '.png'}
        defaultconf['INPUT'] = {'tscale': 1.0,
                                'order_polynomial_drift_correction': 1,
                                'dimension': 2}
        return defaultconf
    
    config = ConfigParser(allow_no_value=True)
    config.read(filename)
    return config

    
def Plot(x, y, xerr=[], yerr=[],
         fN=1, fmt="+",
         markersize=8, alpha=1.0,
         xlabel=r"$\omega$, 1/s", ylabel="G', G'', Pa",
         title="", dpi=150,
         logplot=False, ext=".png", filename="",
         outpath="./", newplot=True, **args):
    """ Generalized plot function for rheology data.
        Parameters:
        x,y             the data set to be plotted
        xerr, yerr:     if present then errorbars to be added
        fN:             figure number (1-10)
        fmt:            format text, typical 'r+' or similar
        markersize:     the size of the symbol, default is 8 points
        xlabel, ylabel: axis labels, with LaTeX type math is possible
        title:          plot title (if specified)
        dpi:            if filename specified, the resolution of the exported
                        image
        logplot:        double log plot or not
        ext:            extension of the output
        filename:       if not empty, then save the plot into this file
                        if loplot specified, then loglog has to be in the
                        filename, or is appended
        outpath:        path of the output
        newplot:        erase the plot before plotting, or just add the 
                        data to an existing figure
        
        any further named arguments are passed to plot() or errorbar().
    """

    if len(x) != len(y):
        print("X and Y lenght differ!")
        return

    #figure(1) is always what we use
    if fN < 1 or fN > 10:
        fN = 1

    fig1 = plt.figure(fN)
    
    #since we always use the subplot(1,1,1), we need to erase it 
    #for a new plot:
    if newplot:
        fig1.clf()

    plt1 = fig1.add_subplot(1,1,1)

    if logplot:
        plt1.set_xscale('log')
        plt1.set_yscale('log')
    else:
        plt1.set_xscale('linear')
        plt1.set_yscale('linear')


    if len(x) > 0:
        if len(xerr) == 0 or len(yerr)== 0:
            plt1.plot(x,y, fmt,\
                markersize=markersize, alpha= alpha, **args)

        else:
            plt1.errorbar(x,y, yerr, xerr, fmt=fmt,\
                markersize=markersize, alpha= alpha, **args)
    #do not plot anything for empty data
    #we still can export the figure though 8)

    #label the axis:
    plt1.set_xlabel( xlabel )
    plt1.set_ylabel( ylabel )

    if title != "":
        plt1.set_title( title )
    
    if filename != "":
        if logplot and "loglog" not in filename:
            filename = "%s-loglog" %filename
        #end if logplot

        fn = os.path.splitext(filename)[0]
        if "." in ext:
            fn = "%s%s" %(fn, ext)
        else:
            fn = "%s.%s" %(fn, ext)
        #end if ext
        plt.savefig( os.path.join(outpath, fn), dpi=dpi)
    #end if filename
    plt.draw()


def LogBinAvg(x, N=50):
    """ Calculate the local average of a data set in a set of centers arranged
        logarithmically.
        The data set is considered equidistant!

        Parameters:
        x:      a Numpy vector
        N:      number of bins

        Be careful, for complex numbers the standard deviation is 
        always real! (see numpy documentation)

        Return: 
        a dict containing the average and the standard deviation
        'avg':      averages
        'std':      standard deviations
        'N':        number of data in the given bin

        The routine drops duplicates, so the result 
        may be shorter than N elements!
    """

    Nx = len(x)
    M = np.log(np.float(Nx))
    lbins = np.arange(M/N,M, M/N)
    bins = np.exp(lbins).astype(np.int)

    if (bins[1:]-bins[:-1]).any() < 1:
        print("Too many bins, use longer array or less bins!")
    
    bins = np.concatenate([bins, [Nx]])

    newx = []
    stdx = []
    Nx = []

    io = 0
    #build up a new array, but drop duplicates!
    for i in bins:
        if i == io:
            continue
        else:
            actual = x[io:i].mean()

            newx.append(actual)
            stdx.append(x[io:i].std())
            Nx.append(i-io)
            io = i
    #end for
    return {'avg':  np.asarray(newx),\
            'std':  np.asarray(stdx),\
            'N':    np.asarray(Nx) }



################## More specific functions (for MSD, J ...) ##################3

def ReadTable(filename, beadindx=-1, X=0, Y=1, indx=2, skip=0):
    """ Read a table from a text file. It considers a white spaced
        text table with a filled up rectangular data set (no missing
        points in the table).

        Parameters:
        beadindx:   which column to use to factorize?
        X:          index of x coordinate
        Y:          index of y coordinate
        indx:       index of image index column 
                    (proportional to the time)

        skip:       how many lines to skip at the beginning

        Return:
        a poslist list of dicts (see ImageP track results)
        Each dict holds 'X','Y'(if any), 'indx' keys, from which
        'X','Y' are coordinate arrays, 'indx' is the image index (time).
        The position in poslist is the index of the trajectory (bead).
    """
    if os.path.isfile(filename):
        fp = open(filename, 'rt')
    else:
        print("file not found: %s" %filename)
        return None
    
    a = fp.readlines()
    fp.close()

    #strip the skip part:
    a = a[skip:]
    Ni = len(a)
    #estimate the number of columns from the first row:
    t = (a[0].strip()).split()
    Nj = len(t)

    #Now read up the table:
    res = []
    for i in range(Ni):
        t = a[i].strip()
        
        l = []
        t = t.split()
        for j in range(Nj):
            l.append(float(t[j]))

        res.append(l)
    #end for i
    print("Found: %d lines" %len(res))

    #congvert to poslist:
    res = np.asarray(res)
    #how many beads? beadindx is the column holding the counter:
    Nb = int(res[:,beadindx].max())
    print("Max: %d beads" % Nb)

    poslist = []
    for i in range(Nb+1):
        bindx = (res[:,beadindx] == i)
        poslist.append({"X":res[bindx,X], "Y":res[bindx,Y],\
                        "indx":res[bindx,indx]})

    print("Found: %d beads" % len(poslist))

    return poslist


def ReadPickle(filename):
    """ pick up a dumped python data file
        filename is forced to have an extension of .dat.
    """
    #forcing extension of .dat:
    fn, ext = os.path.splitext(filename)
    fn = "%s.dat" %fn

    if os.path.isfile(fn):
        fp = open(fn, 'rb')
    else:
        print("file not found: %s" % filename)
        return None

    try:
        a = load(fp)
    except:
        print("Unable reading pickle!")
        a = None

    fp.close()
    return a

    
################ CALCULATION ROUTINES ###################
#########################################################

def do_histogram(
        a, density: bool=True
) -> Tuple[plt.Figure, plt.Axes]:
    """ generate a position histogram from the data, and export
        the results to a plot and data files

        Parameters:
        a:          the position data array
        dpi, ext:   passed to Plot
        norm:       normalize the histogram
        normwith:   if 1 then normalize with the sum,
                    else normalize using a trapezoid integral
                    sum of the histogram
    """
    fig, ax = plt.subplots(1, 1)
    
    for j in range(a.shape[1]):

        h, boundaries = np.histogram(a[:, j], 50, density=density)

        x = (boundaries[1:] + boundaries[:-1]) * 0.5

        ax.plot(x, h, '-+', alpha=0.3)
        
    return fig, ax


def Do_MSD(x, a, i, config, outpath="./", logger=None,
           dpi=150, verbose=True, ext=".png" ):
    """ And all others, such as:
        MSD, MKD, and power fitting if specified

        Parameters:
        x, a:       time data and position array of a bead
        i:          the bead's ID (number)
        config:     dict containing the configuration paramters
        rep:        report class to send report to
        dpi:        resolution of exported graphs
        verbose:    talk?
        ext:        extension of exported graphs

        Returns:
        msd[]:      an MSD dict, containing the MSD object
                    and the fitted data
                    All others are saved and dumped into files
    """
    #control parameters from the config:
    cmds = [k.upper() for k in config['COMMANDS'].keys()]
    newplot = True
    t_0 = float(config['INPUT']['t0']) if 't0' in config['INPUT'] else -1.0
    tolerance = float(config['MSD']['tolerance']) if 'tolerance' in config['MSD'] else 0.1
    MKD_k = int( config['MSD']['MKD_k']) if 'MKD_k' in config['MSD'] else 4.0

    #overlap or not in the MSD calculations:
    if 'overlap' in config['MSD']:
        overlap = True
    else:
        overlap = False

    #MSD filename to be picked up:
    msdfile = config['MSDfile'][-1] if 'MSDfile' in config else ""

    #fit parameters:
    powerfit = True if "powerfit" in cmds else False

    if powerfit:
        config_fit = config['FIT POWER LAW']
        FillRatio = float(config_fit['fill']) \
            if 'fill' in config_fit else 0.9
        Scaler = float(config_fit['scaler']) \
            if 'scaler' in config_fit else 10.0
        scalermode = config_fit['scalermode']

        Smoothing = int(config_fit['smoothing']) \
            if 'smoothing' in config_fit else -1.0
        OverSampling = float(config_fit['OverSampling']) \
            if 'OverSampling' in config_fit else -1.0
        eps = float(config_fit['epsilon']) if 'epsilon' in config_fit else 1E-4
        InsertN = int(config_fit['InsertN']) \
            if 'InsertN' in config_fit else 0

        KelvinStart = True if 'KelvinStart' in config_fit else False

    logger.info("\nTesting MSD/... calculations")
    logger.info("Specific parameters:")
    logger.info( ["Tolerance is set to: ", tolerance])
    logger.info( ["Order of MKD and MMKE (k):", MKD_k])

    #end if rep
    if "MSD" in  cmds or "J" in cmds or "G" in cmds:

        #MSD text and filename:
        txt = "MSD" if not overlap else "MSD-with-overlap"

        #we have to override fn with the proper output filename:
        fn = "%s-%d" % (txt, i)

        logger.info( "Calculating MSD")

        ms = MSD(a, 0, x, tolerance=tolerance,
                 overlap=overlap)

        logger.info("done.")

        #plot MSD in loglog and linear scales
        if 'DMSD' in ms and 'dtau' in ms:
            Plot(ms['tau'], ms['MSD'], xerr=ms['dtau'], yerr=ms['DMSD'],
                 fmt='r+', alpha=0.3, newplot=True,
                 xlabel="$\\tau$, seconds",
                 ylabel=" MSD, MME, $\mu m^2$",
                 outpath=outpath, dpi=dpi, filename=fn, ext=ext)

            Plot( ms['tau'], ms['MSD'], xerr=ms['dtau'], yerr=ms['DMSD'],
                  fmt='r+', alpha=0.3, newplot=True, fN=2, logplot=True,
                  xlabel="$\\tau$, seconds", 
                  ylabel=r"MSD, MME, $\mu m^2$",
                  outpath=outpath, dpi=dpi, filename=fn, ext=ext)
            newplot = False
        else:
            Plot(ms['tau'], ms['MSD'],
                 fmt='r+', alpha=0.3, newplot=True,
                 xlabel="$\\tau$, seconds",
                 ylabel=" MSD, MME, $\mu m^2$",
                 outpath=outpath, dpi=dpi, filename=fn, ext=ext)

            Plot(ms['tau'], ms['MSD'],
                 fmt='r+', alpha=0.3, newplot=True, fN=2, logplot=True,
                 xlabel="$\\tau$, seconds",
                 ylabel=" MSD, MME, $\mu m^2$",
                 outpath=outpath, dpi=dpi, filename=fn, ext=ext)
            newplot = False

        logger.info("plotting MSD - red")

    #####################################################################
    newplot = True
    #MKD: mean kth power displacement, used to be MMK for mistake
    if "MKD" in cmds or "MMK" in cmds:
        logger.info( [MKD_k, "-th momentum"])

        msk = MKD(a, MKD_k, 0, x, tolerance=tolerance,
                  overlap=overlap, MME=False)

        txt = "M%dD" % MKD_k

        if overlap:
            fn = "%s-with-overlap" % txt
        else:
            fn = "%s" % txt

        fn = "%s-%d" % (fn, i)

        Plot(msk['tau'], msk['MKD'], fmt='r+', fN=1, alpha=0.3,
             newplot=newplot,
             xlabel="$\\tau$, seconds", ylabel=txt,
             dpi=dpi, filename=fn, outpath=outpath, ext=ext)

        Plot(msk['tau'], msk['MKD'], fmt='r+', fN=2, alpha=0.3, newplot=newplot,
             xlabel="$\\tau$, seconds", ylabel=txt, logplot=True,
             dpi=dpi, filename=fn, outpath=outpath, ext=ext)
        newplot = False # for the MMKE plot

        logger.info( "plotting MKD - red" )

        #theory predicts (Tejedor et al), that the
        #ratio between MKD/MSD should be constant
        #create this ratio and spit it out:
        if "MSD" in cmds:
            logger.info("Ratio of MKD/MSD")
            y = msk['MKD'] / (ms['MSD']*ms['MSD'])

            txt = "M%dD-per-MSD-bead" % MKD_k
            if overlap:
                txt = "%s-with-overlap" % txt

            fn = "%s-%d" % (txt, i)
            txt = "Ratio of M%dD/MSD^2 for bead %d" % (MKD_k, i)

            txt = "Ratio of $M%dD/MSD^2$ for bead %d" % (MKD_k, i)
            Plot(ms['tau'], y, fmt='r+', fN=3, newplot=True, logplot=True,
                 xlabel="$\\tau$, seconds",
                 ylabel=txt,
                 filename=fn, outpath=outpath, dpi=dpi, ext=ext)
            plt.close(3)

    if "MMKE" in cmds:
        logger.info([MKD_k, "-th momentum"])

        msk = MKD(a, MKD_k, 0, x, tolerance=tolerance,
                  overlap=overlap, MME=True)

        txt = "M%dME" % MKD_k
        if overlap:
            fn = "%s-with-overlap" % txt
        else:
            fn = "%s" % txt
        #end if
        fn = "%s-%d" % (fn, i)

        #label to the plot:
        txt = r"MKD, MM%dE, $\mu m^%d$" % (MKD_k, MKD_k)

        if not newplot:
            fn = "MKD-%s" % fn

        Plot(msk['tau'], msk['MKD'], fmt='b+', fN=1, alpha=0.3, newplot=newplot,
             xlabel="$\\tau$, seconds", ylabel=txt,
             dpi=dpi, filename=fn, outpath=outpath, ext=ext)

        #loglog:
        Plot(msk['tau'], msk['MKD'], fmt='b+', fN=2, alpha=0.3, newplot=newplot,
             logplot=True,
             xlabel="$\\tau$, seconds", ylabel=txt,
             dpi=dpi, filename=fn, ext=ext)

        logger.info( "plotting MKE - blue")
        logger.info("Plot created")

    if "powerfit" in  cmds and "MSD" in cmds:
        logger.info(["\nPowerfit is set, parameters are:"])
        logger.info(["Starting time point is", t_0, "seconds"] )
        logger.info( ["Scaler (used in i1= i0*scaler) is", Scaler])

        if scalermode not in scalermodes:
            logger.info("Invalid scaler mode, setting back to 'scaler'")
            scalermode = 'scaler'

        logger.info( ["Scaler mode is set to", scalermode])

        if OverSampling == -1:
            logger.info( ["Oversampling fit with automatic factor"])
            logger.info( ["Sampling precision is set to", eps])
        else:
            logger.info( ["Oversampling fit with a factor of", OverSampling])

        logger.info( ["Insert", InsertN, "points between tau[0] and 0"])
        logger.info( ["Fill ratio of the fit is", FillRatio ])
        if KelvinStart:
            logger.info("Power fitting would use a Kelvin model at the beginning of the data")
            
        if Smoothing > -1:
            logger.info(["Smoothing is activated, range is set to",\
                        Smoothing,"data points"])

        logger.info( "fitting power function to the MSD" )

        #MSD_power_fits do the plotting as well:
        fits = MSD_power_fit(ms, t_0, fill=FillRatio,
                             scaler=Scaler,
                             Kelvin=KelvinStart,
                             mode=scalermode,
                             verbose=True)

        #save the parameters:
        fitlist = []
        for fi in fits:
            fitlist.append([fi['a'][0], fi['b'][0], fi['t0'],
                            fi['t1'], fi['i1']-fi['i0'] ])

        #end for fi
        fn = "MSD-power-fit-parameters-%d" % i
                
        #Figures of the fit:
        #these figures are the output of the power interpolation
        #MSD_power_fit, verbose part
        plt.figure(2)
        fn = "MSD-power-interpolated-%d%s" %(i, ext)
        fn = os.path.join(outpath, fn)
        plt.savefig(fn, dpi=dpi)
        plt.close()
        
        plt.figure(1)
        fn = "MSD-power-interpolated-loglog-%d%s" %(i, ext)
        fn = os.path.join(outpath, fn)
        plt.savefig(fn, dpi=dpi)
        plt.close()

        #Now interpolate based on the fits:
        ms2 = MSD_interpolate(ms, fits,
                              smoothing=True, Nsmoothing=Smoothing,
                              insert=InsertN,
                              factor=OverSampling,
                              eps=eps,
                              verbose=True)

        fn = "MSD-power-interpolated-%d" % (i)
        txt = "Power interpolated MSD, fitted by %d curves" % len(fits)

        #Plot the fitted MSD on the original one:
        Plot(ms['tau'], ms['MSD'], fmt='b+')
        Plot(ms2['tau'], ms2['MSD'], fmt='r-',
             xlabel="$\\tau$, seconds",
             ylabel="MSD, $\mu^2$", newplot=False,
             filename=fn, outpath=outpath, ext=ext,
             dpi=dpi, logplot=False)

        logger.info(["MSD interpolation is plotted to:", fn])
        #end of fitting MSD

        ms['fitted'] = ms2
        ms['fits'] = fits

    return ms

def Do_J(ms, i, config, D=2.0, outpath="./", logger=None,
         dpi=150, verbose=True, ext=".png" ): 
    """ Do all the calculation and plotting things about
        creep compliance from the msd data

        Parameters:
        ms:     an MSD dict from the Do_MSD
        i:          the bead's ID (number)
        config:     dict containing the configuration paramters
        D:          dimensions: this may change, for example when
                    orientational motion is taken (D=1)
        rep:        report class to send report to
        dpi:        resolution of exported graphs
        verbose:    talk?
        ext:        extension of exported graphs

        Return:
        J:      a dict containing the creep compliance
    """
    config_msd = config['MSD']
    Temp = float(config_msd['Temp']) if 'Temp' in config_msd else 22.0
    radius = float(config_msd['radius']) if 'radius' in config_msd else 1.0
    t_0 = float(config_msd['t0']) if 't0' in config_msd else -1.0
    t_end = float(config_msd['tend']) if 'tend' in config_msd else -1.0
    ForceJ0 = float(config_msd['ForceJ0']) if 'ForceJ0' in config_msd else -1.0

    #fit parameters:
    powerfit = True if 'fitted' in ms else False

    if powerfit:
        KelvinStart = True if 'KelvinStart' in config['FIT POWER LAW'] else False

    #spit out some information:
    logger.info("\nCalculating the creep compliance")
    logger.info("Specific parameters:")
    logger.info(["Particle size is",
                2.0*radius, "microns, radius is", radius, "microns"])
    logger.info(["Treating as a(n)", D, "dimensional problem"])
    logger.info( ["Temperature is:", Temp, "deg. Celsius"])
    logger.info( ["J0 fitted up to", t_0, "seconds"])
    logger.info( ["eta is fitted from", t_end, "to end"])
    #end of reporting

    #if t_0 and t_end are not well defined, we need an input:
    if t_0 < 0 or t_end < 0:
        logger.info("We need the fitting ranges defined!")
        print("Please have a look at which part can one fit a straight line to J(tau)")

        if t_0 < 0:
            JN = min(1000, ms['tau'].size)
            Plot(ms['tau'][:JN], ms['MSD'][:JN], fmt='+',
                 xlabel="t, sec.", ylabel="MSD $\mu m^2$",
                 newplot=True)
                
            txt = input("t_0?: ")
            t_0 = float(txt)
            logger.info(["Enter config t0:", t_0])

        if t_end < 0:
            JN = max(0, ms['tau'].size - 1000)
            Plot(ms['tau'][JN:], ms['MSD'][JN:], fmt='+',
                 xlabel="t, sec.", ylabel="MSD $\mu m^2$",
                 newplot=True)
                
            txt = input("t_0?: ")
            t_end = float(txt)
                
            logger.info(["Enter config tend:", t_end])

    #now do the conversion:
    J = MSD_to_J(ms, t_0, t_end,
                 T=Temp, 
                 a=radius,
                 D=D, verbose=True)

    logger.info(["J(t) conversion constant:", J['const']])
    logger.info(["Eta fitted (Pas)", J['eta']])
    logger.info(["J0 (1/Pa) fitted:",J['J0']])
    logger.info(["First data J[0]:", J['J'][0]])

    if 'dJ' in J and 'dtau' in J:
        Plot(J['tau'],J['J'],xerr=J['dtau'], yerr=J['dJ'],\
             fmt='r+', xlabel='$\\tau$, seconds',\
             ylabel='Creep compliance, 1/Pa',\
             outpath= outpath, filename='dJ-bead-{}'.format(i), dpi= dpi,\
             ext= ext, newplot=True)
        
        Plot(J['tau'],J['J'],xerr=J['dtau'], yerr=J['dJ'], logplot=True,\
             fmt='r+', xlabel='$\\tau$, seconds',\
             ylabel='Creep compliance, 1/Pa',\
             outpath= outpath, filename='dJ-bead-{}-loglog'.format(i), dpi= dpi,\
             ext= ext, newplot=True)
    else:
        Plot(J['tau'],J['J'],\
             fmt='r+', xlabel='$\\tau$, seconds',\
             ylabel='Creep compliance, 1/Pa',\
             outpath= outpath, filename='J-bead-{}'.format(i), dpi= dpi,\
             ext= ext, newplot=True)

        Plot(J['tau'],J['J'],\
             fmt='r+', xlabel='$\\tau$, seconds',\
             ylabel='Creep compliance, 1/Pa',\
             outpath= outpath, filename='J-bead-{}-loglog'.format(i), dpi= dpi,\
             ext= ext, newplot=True)
    
    #Kelvin Start should force J0 = 0 for the fitted data, but not the original!
    if ForceJ0 >= 0.0:
        logger.info(["Forcing J0 to:", ForceJ0] )
        J['J0'] = ForceJ0

    if powerfit and 'fitted' in ms:
        ms2 = ms['fitted'] 

        if 'fits' in ms:
            fits = ms['fits']
        else:
            logger.info("Fitting is specified, but not done! ERROR")
            logger.info("Stop processing J here, and return")
            return J
        #Change the end time according to fit limitations:
        # (one can limit the end time of the fit...)
        #report parameters specific to the power fit only:
        logger.info("\nConverting the fitted MSD to Creep compliance")
        #logger.info( ["0 -", FillRatio, "of the data is used for power fit"])
        #logger.info( ["Scaler (used in i1= i0*scaler) is", Scaler])

        #the end segment has to be readjusted to the fitted time range:
        t_end_new = ms2['tau'].max() - (ms['tau'].max() - t_end)

        #this is a crude treatment of the case:
        if t_end_new < t_0:
            t_end_new = t_0 + 0.1

        J2 = MSD_to_J(ms2, t_0, t_end_new,
                      T=Temp,
                      a=radius,
                      D=D,
                      verbose=True)

        if KelvinStart:
            Emodulus = 1.0/(fits[0]['a']*J['const']) \
                if fits[0]['a'] != 0.0 else 0.0
            Eta = Emodulus/fits[0]['b'] if fits[0]['b'] != 0.0 \
                else 0.0
            logger.info("Kelvin fit was applied at high frequencies (low t)")
            logger.info(["Kelvin fit Young modulus (E):",Emodulus, "Pa"])
            logger.info(["Kelvin fit viscosity (eta):",Eta,"Pas"])
            logger.info("Forcing J0 = 0.0 1/Pa because KelvinStart is enabled")

            J2['J0'] = 0.0

        elif ForceJ0 >= 0.0:
            logger.info(["Forcing J0 in interpolated J to", ForceJ0])
            J2['J0'] = ForceJ0
        #end if KelvinStart or ForceJ0>=0.0

        #store the fitted J into the original dict:
        J['fitted'] = J2
        #end if J in cmds
    #end if powerfit in cmds

    logger.info("\n")

    return J
#end of Do_J

def Do_G(J, i, config, outpath="./", logger=None,
         dpi=150, verbose=True, ext=".png" ): 
    """ Do all the calculation and plotting things about
        the complex shear modulus G.

        Parameters:
        J:          the creep compliance dict from Do_J
        i:          the bead's ID (number)
        logger:     logger object
        dpi:        resolution of exported graphs
        verbose:    talk?
        ext:        extension of exported graphs

        Return:
        J:      a dict containing the creep compliance
    """
    cmds = [k.upper() for k in config['COMMANDS'].keys()]
    DoMason = True if "Mason" in cmds else False

    config_mason = config['MASON']
    #AdvancedMason: use the advanced version of Dasgupta et al.
    AMason = True if 'AdvancedMason' in config_mason else False
    logMason = False if 'linMason' in config_mason else True
    wMason = float(config_mason['wMason']) if 'wMason' in config_mason else 1.0
    rMason = int(config_mason['rMason']) if 'rMason' in config_mason else 5
    NMason = int(config_mason['NMason']) if 'NMason' in config_mason else 50

    #if it is Mason method, there are parameters to dump:
    if DoMason:
        logger.info(["Using Mason type of conversion to G instead of fitting"])

        if AMason:
            logger.info(["Employing the advanced, improved Mason fit"])

        logger.info(["Parameters:"])
        logger.info(["Number of omega values:", NMason])
        logger.info(["Radius of window:", rMason, "width of Gaussian:", wMason])
        if logMason:
            logger.info(["Use logarithmic sampling"])
        else:
            logger.info(["Use linear sampling"])

    #end if DoMason

    logger.info("\nCalculating G")

    G = J_to_G(J)

    txt = "G of bead %d" % i
    fn = "G-bead-%d" % i

    #Now we can also dump a logbinned avg (asked by Referee 2):
    #we split it up, so we get an independent error estimate on
    #the real and imaginary parts:
    binomg = LogBinAvg(G['omega'], 50)
    binGr = LogBinAvg(G['G'].real, 50)
    binGi = LogBinAvg(G['G'].imag, 50)

    #We fall back to converting the interpolated 
    #J to G, because the interpolated, known analytical
    #functions do not convert easily to G 
    #(J is not the linear combination of them!)
    #if we have a smoothed J, which we need for G2
    
    if DoMason:
        if 'fitted' in J:
            G2 = J_to_G_Mason(J['fitted'],
                              N=NMason,
                              r0=rMason,
                              w=wMason,
                              logstep=logMason,
                              advanced=AMason,
                              verbose=verbose)
            txt = "J fitted"
            fn = "MasonFitted-G-bead-%d" % i
        else:
            G2 = J_to_G_Mason(J,
                              N= NMason,
                              r0= rMason,
                              w= wMason,
                              logstep= logMason,
                              verbose= verbose,
                              advanced= AMason)
            txt = "J"
            fn = "Mason-G-bead-%d" % i

    elif 'fitted' in J:
        G2 = J_to_G(J['fitted'],omax = G['omega'].max())

    logger.info("Plotting complex G: real - red, imaginary part- blue")
    fn = "G-of-bead-%d" % i
    Plot(G['omega'], G['G'].real, fmt='r+', alpha=0.3)
    Plot(G['omega'], G['G'].imag, fmt='b+', alpha=0.3, newplot=False,
         xlabel=r"$\omega$, 1/s", ylabel="G', G'', Pa")

    #if there is some kind of fit, plot it:
    if 'fitted' in J or DoMason:
        Plot(G2['omega'], G2['G'].real, fmt='k-', newplot=False)
        Plot(G2['omega'], G2['G'].imag, fmt='g-', newplot=False)

    #Save the plots:
    #but separate the Mason fit from the usual one.
    if DoMason:
        fn = "Mason-%s" % fn

    Plot([], [], newplot=False,
         filename=fn, outpath=outpath, dpi=dpi)
    logger.info(["G linear graph saved as:", fn])

    #now the loglog graphs:
    logger.info("Creating log(abs(G)) graph")
    Plot(G['omega'], np.abs(G['G'].real), fmt='r+', alpha=0.3)
    Plot(G['omega'], np.abs(G['G'].imag), fmt='b+', alpha=0.3, newplot=False,
         xlabel=r"$\omega$, 1/s", ylabel="G', G'', Pa")

    #add the logsmoothed part:
    Plot(binomg['avg'], binGr['avg'],\
         fmt="m-o", markersize=4, newplot=False)
    Plot(binomg['avg'], binGi['avg'],\
         fmt="c-o", markersize=4, newplot=False)

    if 'fitted' in J or DoMason:
        Plot(G2['omega'],np.abs(G2['G'].real), fmt= 'k-', newplot=False)
        Plot(G2['omega'],np.abs(G2['G'].imag), fmt= 'g-', newplot= False)

        G['fitted'] = G2
    #Save the plots:
    Plot([],[], newplot= False, logplot=True,\
         filename= fn, outpath= outpath, ext= ext, dpi= dpi)

    logger.info("abs(G) log-log graph saved")

    return G
