#!/usr/bin/env python
""" Do a general rheology analysis of particle tracks.
    Problems:
        - tracks can come in various formats
                -- text tables
                -- pickle objects
        - generate graphic output
        - generate pickle file dumps and text tables
        - write a report collecting information and results
    
    Author:     Tomio
    Date:       2011 May - July - 
    Warranty:   None
    License:    LGPL
    version:    0.2
"""

import sys
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from Rheology import GetData, TimeAverageShift, RotateDataset
from Rheology.Process import (read_conf, do_histogram, Plot, Do_MSD,
                              Do_J, Do_G)

import logging


if __name__ == "__main__":

    log_fh = logging.FileHandler('log.txt')
    log_ch = logging.StreamHandler()
    log_formatter = logging.Formatter('%(levelname)s:%(message)s')
    log_fh.setFormatter(log_formatter)
    log_ch.setFormatter(log_formatter)
    log_fh.setLevel(logging.INFO)
    log_ch.setLevel(logging.INFO)
    logger = logging.getLogger('ProcessRheology')
    logger.setLevel(logging.INFO)
    logger.addHandler(log_fh)
    logger.addHandler(log_ch)
        
    configfile = "config.txt"

    args = sys.argv
    if len(args) > 1:
        if not os.path.isfile(args[1]):
            msg = "Invalid config file name: %s" % args[1]
            raise FileNotFoundError(msg)
        configfile = args[1]

    #get configuration:
    
    if not os.path.isfile(configfile):
        msg = "No config file found. Exiting."
        raise RuntimeError(msg)

    config = read_conf(configfile)

    pathname = os.path.abspath(config['PATHS']['dir']) 
    outpath = config['PATHS']['outdir']
    outpath = pathname if outpath == "dir" else os.path.abspath(outpath)
    
    ###### General/global parameters:
    #resolution of exported plots:
    dpi = int(config['PLOTS']['dpi'])
    ext = config['PLOTS']['ext']
    verbose = bool(int(config['DEFAULT']['verbose']))

    D = float(config['INPUT']['dimension']) \
        if 'dimension' in config['INPUT'] else 2

    order = int(config['INPUT']['order_polynomial_drift_correction']) \
        if 'order_polynomial_drift_correction' in config['INPUT'] else 1
    #physical scaling of positions:
    scale = float(config['INPUT']['microns_per_pixel'])

    ## Histogram parameters:
    histogram_density = bool(int(config['PLOTS']['histogram_density']))\
        if 'histogram_density' in config['PLOTS'] else True
    
    #some output about the general parameters:
    logger.info( ["Image scale is:", scale])
    logger.info( [ "Order of drift correction is:", order])

    # load time stamps in any way you like
    
    tfile = config['INPUT']['timestamp']
    print("Reading timestamp file:", tfile, "...", end=' ')
    timestamps = np.loadtxt(tfile, skiprows=1, usecols=[1])

    data_file = config['INPUT']['data_file']

    # HANDLE LOADING OF TRAJECTORIES ....
    trajectories = {}

    df = pd.read_csv(data_file, delim_whitespace=True, header=None)
    df.columns = ['x', 'y', 'frame', 'bead_index', 'frame2']

    if 'beads' in config['INPUT']:
        beads = []
        bsplit = config['INPUT']['beads'].split(',')
        for bid in bsplit:
            beads.append(int(bid))
    else:
        beads = list(df['bead_index'].unique())
            
    for bid in beads:
        trajectories[bid] = df[df['bead_index'] == bid]
    
    logger.info("Data of", len(trajectories), "particles loaded")

    if len(trajectories) == 0:
        msg = 'no particle data was loaded; are there data in the file you specified'
        msg += ' ({})?'.format(data_file)
        raise RuntimeError(msg)

    cmds = [k.upper() for k in config['COMMANDS'].keys()]
    #we should not contaminate the command list:
    txt = cmds.copy()
    txt.insert(0, "Command list:")
    logger.info(txt)

    fname_hist_template = os.path.join(outpath, 'hist_bead_{:03}' + ext)

    #Now loop through the particles:
    for bid in beads:
        logger.info(["Taking particle", bid])

        if len(trajectories[bid]) < 2:
            logger.info(["Empty data set", bid, "skipping it"])
            continue

        x, a = GetData(timestamps, trajectories[bid], order=order,
        resolution=scale, logger=logger)

        logger.info(["Data with length of", len(a), "selected"])

        #Fundamental statistics, histogram:
        fig, ax = do_histogram(a, density=histogram_density)
        fig.savefig(fname_hist_template.format(bid))

        #histogram of a step size:
        dat = TimeAverageShift(a, 1)
        h, boundaries = np.histogram(dat['RSQ'], 50)
        mids = (boundaries[1:] + boundaries[:-1]) * 0.5

        #plot the stepsize histogram:
        fn = "step-histogram-bead-%d" % bid
        Plot(mids, h, fmt='-+', alpha=0.3,
             xlabel="Step size, $\mu^2$", ylabel="Count",
             filename=fn, outpath=outpath, ext=ext,
             dpi=dpi, newplot=True)

        #bead trajectories vs time:
        for k in range(a.shape[1]):
            newplot = True if k == 0 else False
            Plot(x, a[:, k], fmt='+', alpha=0.3, newplot=newplot)
        fn = "trajectory-bead-%d" % bid
        Plot([], [], newplot=False, xlabel="time, seconds",
             ylabel="Position, $\mu$m",
             filename=fn, outpath=outpath, ext=ext, dpi=dpi)

        #a default MSD length:
        Nx = len(x)/4

        newplot = True
        #We should loop through the particles and do the following:
        if "reorient" in cmds:
            #Do an orientation test using a "inertia momentum tensor"
            logger.info("Anisotropy analisis of positions")

            rota = RotateDataset(a, convert=True)
            logger.info(["Major axis:", rota['Eigenvector0'][0], rota['Eigenvector0'][1]])
            logger.info(["Minor axis:", rota['Eigenvector1'][0], rota['Eigenvector1'][1]])
            logger.info(["Major axis length:", rota['MajorAxis']])
            logger.info(["Minor axis length:", rota['MinorAxis']])

            ell = rota['MinorAxis']/rota['MajorAxis'] 
            logger.info(["Minor/major axis ratio:", ell])
            alpha = float( int(rota['alpha']*180.0/np.pi*100)/100.0)
            logger.info(["Angle of rotation:", alpha, "degrees"])

            if ell < 0.9:
                logger.info("The data set is anisotropic")

                b = rota['NewData']

                #first repeat the histogram, overwrite the old:
                fig, ax = do_histogram(b, density=histogram_density)
                fig.savefig(fname_hist_template.format(bid))

                Plot(a[:, 0], a[:, 1], fmt='g+', newplot=True)
                Plot(b[:, 0], b[:, 1], fmt='y+', newplot=False)
                plt.axis('equal')
                c = rota['Center']
                ma = rota['MajorAxis']*rota['Eigenvector0']
                #plot 2x the axis, because that is like the STD of the data, 2x STD is 95%
                #of the data points
                Plot([c[0], c[0]+2*ma[0]], [c[1], c[1]+2*ma[1]], fmt='r-', newplot=False,
                     outpath=outpath, dpi=dpi, filename="Position-distribution-XY",
                     ext=ext)

                '''
                #we need to export the data:
                fn = "Rotated-positions-%d" %bid
                txt = "Positions rotated to alpha: %.1f degrees" %alpha
                SaveData( ['time, seconds','X, microns','Y, microns'],\
                            zip(x,b[:,0],b[:,1]),\
                            os.path.join(outpath, fn),\
                           txt) 
                '''

                #we need a dir for X and Y directions
                dirlist = ["Xcomp", "Ycomp"]
                compname = ['X', 'Y']
                for k in [0, 1]:
                    logger.info(["Analyzing component", compname[k]])

                    dirname = os.path.join(outpath, dirlist[k])
                    if not os.path.isdir(dirname):
                        os.mkdir(dirname)
                    os.chdir(dirname)

                    #now process them:
                    #do the same as for the whole 2D trajectory,
                    #but here for the components only
                    #we need a modified output dir, and 1D
                    ms = Do_MSD(x, b[:, k], bid, config, dirname, logger,
                                dpi, verbose, ext)

                    if "J" in cmds or "G" in cmds:
                        J = Do_J(ms, bid, config, 1.0, dirname, logger,
                                 dpi, verbose, ext)

                    if "G" in cmds:
                        G = Do_G(J, bid, config, dirname, logger, dpi, verbose, ext)

                    logger.info([compname[k], "is done"])
                    os.chdir(outpath)
                #end for k

        ms = Do_MSD(x, a, bid, config, outpath, logger,
                    dpi, verbose, ext)

        if "J" in cmds or "G" in cmds:
            J = Do_J(ms, bid, config, D, outpath, logger,
                     dpi, verbose, ext)

        if "G" in cmds:
            G = Do_G(J, bid, config, outpath, logger, dpi, verbose, ext )

        logger.info("Done, next bead please")
    #end for in BL
    logger.info("\nBye and thanks for the fish!")
