#!/usr/bin/env python
from time import time
import os
from matplotlib import pyplot as pl
from numpy.random import normal
import numpy as nu
from scipy.special import gamma
from Rheology import MSD, MSD_to_J, J_to_G, J_to_G_Mason, MSD_power_fit, MSD_interpolate

pl.ion()

#we need to plot a lot:
def Plot( x, y, xerr=[], yerr=[],
          fN=1, fmt="+",
          markersize=8, alpha=1.0,
          xlabel="", ylabel="",
          title="", dpi=150,
          logplot=False, ext=".png", filename="",
          outpath="./", newplot=True, **args):
    """ Generalized plot function for rheology data.
        Parameters:
        x,y             the data set to be plotted
        xerr, yerr:     if present then errorbars to be added
        fN:             figure number (1-10)
        fmt:            format text, typical 'r+' or similar
        markersize:     the size of the symbol, default is 8 points
        xlabel, ylabel: axis labels, with LaTeX type math is possible
        title:          plot title (if specified)
        dpi:            if filename specified, the resolution of the exported
                        image
        logplot:        double log plot or not
        ext:            extension of the output
        filename:       if not empty, then save the plot into this file
                        if loplot specified, then loglog has to be in the
                        filename, or is appended
        outpath:        path of the output
        newplot:        erase the plot before plotting, or just add the 
                        data to an existing figure
        
        any further named arguments are passed to plot() or errorbar().
    """

    if len(x) != len(y):
        print("X and Y lenght differ!")
        return
    #end if

    #figure(1) is always what we use
    if fN < 1 or fN > 10:
        fN = 1

    fig1 = pl.figure(fN)
    
    #since we always use the subplot(1,1,1), we need to erase it 
    #for a new plot:
    if newplot:
        fig1.clf()

    plt1 = fig1.add_subplot(1,1,1)

    if logplot:
        plt1.set_xscale('log')
        plt1.set_yscale('log')
    else:
        plt1.set_xscale('linear')
        plt1.set_yscale('linear')

    if len(x) > 0:
        if len(xerr) == 0 or len(yerr) == 0:
            plt1.plot(x, y, fmt,
                      markersize=markersize, alpha=alpha, **args)

        else:
            plt1.errorbar(x, y, yerr, xerr, fmt=fmt,
                          markersize=markersize, alpha=alpha, **args)
    #do not plot anything for empty data
    #we still can export the figure though 8)

    #label the axis:
    plt1.set_xlabel( xlabel )
    plt1.set_ylabel( ylabel )

    if title != "":
        plt1.set_title( title )
    
    if filename != "":
        if logplot and "loglog" not in filename:
            filename = "%s-loglog" % filename
        #end if logplot

        fn = os.path.splitext(filename)[0]
        if "." in ext:
            fn = "%s%s" % (fn, ext)
        else:
            fn = "%s.%s" % (fn, ext)
        #end if ext
        pl.savefig( os.path.join(outpath, fn), dpi=dpi)
    #end if filename
    pl.draw()

#end of Plot


###############################################################################
# Test Gaussian distribution and MSD: this is an ideal, constant MSD:
dt = 0.01 # seconds time step
tmax = 300 # seconds experiment time
#how to get a width and mean: 
#take N points using standard_normal, multiply with variance, add the mean
X0 = 0.15 # X standard deviation
Y0 = 0.15 # Y standard deviation

#Generate a normal distribution:
tau = nu.arange(dt, tmax, dt)
pos = nu.zeros((tau.shape[0], 2))
#loc: mean value, scale=stdev
pos[:, 0] = normal(size=tau.size, scale=X0, loc=0.0)
pos[:, 1] = normal(size=tau.size, scale=Y0, loc=0.0)

Plot(tau, pos[:, 0], fmt='r+', alpha=0.3)
Plot(tau, pos[:, 1], fmt='b+', alpha=0.3, newplot=False)
#
#MSD:
print("Calculating MSDs from a normal distribution of positions.")
print("This is a slow process, may take several minutes (5 minutes).\n")
print("Calculating MSD with overlap:")
t = time()
msd_overlap = MSD(pos, 0,
                  tvector=tau,
                  tolerance=0.1,
                  overlap=True)
print("done in %.2f seconds" % (time()-t))

print("Calculating MSD without overlap:")
t = time()
msd_no_overlap = MSD(pos, 0,
                     tvector=tau,
                     tolerance=0.1,
                     overlap=False)
print("done in %.2f seconds" % (time()-t))

#print "MME without overlap"
#t = time()
#mme = MSD(pos, 0,\
#        tvector=tau,\
#        tolerance= 0.1,\
#        overlap= False,\
#        MME=True)
#print "done in %.2f seconds" %(time()-t)

#Display the result:
Plot(msd_no_overlap['tau'], msd_no_overlap['MSD'], fN=2,\
     fmt= 'r+', alpha=0.3, logplot=True)
Plot(msd_overlap['tau'], msd_overlap['MSD'], fN=2,\
     fmt= 'b+', alpha=0.3, logplot=True, newplot=False)
Plot(msd_overlap['tau'],nu.zeros(msd_overlap['tau'].size)+\
     2.0*(X0*X0+Y0*Y0),fN=2,\
     fmt= 'g-', alpha=0.3, logplot=True, newplot=False,\
     #        fmt= 'b+', alpha=0.3, logplot=True, newplot=False)
     #Plot(mme['tau'], mme['MSD'], fN=2,\
     #        fmt= 'go', alpha=0.3, logplot=True,newplot=False,\
     xlabel='$\\tau$', ylabel='MSD')

#for normal distribution:
#calculating the MSD=<|r(t)-r(0)|^2> = <|r(t)-<r>|^2 + 2|r(t)-<r>||<r>-r(0)| + 
#|<r>-r(0)>^2 = 2 var(XY) = 2(var(X)  + var(Y) = 2(X0^2+Y0^2)
print("MSD should be a plateau at about %.4g (green line)" %(2*(X0*X0+Y0*Y0)))
input("Press ENTER to continue")
#
#Now do some tests with J and G:
ND = 2 # 2D motion
a = 1  # particle with 1 micron radius
T = 23 # C degrees, temperature
t0 = 0.2 # seconds
tend = 100 # seconds

kB = 1.3806504 
Const = 3.0 * nu.pi * a * 100000.0 / (ND*kB*(T+273.15))
MSD = dict()
MSD['tau'] = tau

#power law J: a x^b type.
E = 10.0 # Pa
b = 0.75 # nice slope
MSD['MSD'] = 1.0/(E*Const)*(tau**b)

#Plot(MSD['tau'], MSD['MSD'], fN=1, \
#        xlabel='$\\tau$, sec.', ylabel='MSD, $\mu m^2$', logplot=True)

J = MSD_to_J(MSD, t0, tend, T, a, ND, verbose=True)

print("\n\nCreep compliance is calculated for power law MSD")
Plot(J['tau'], J['J'], fN=3, \
        xlabel='$\\tau$, sec.', ylabel='J, 1/Pa', logplot=True)
Plot(tau, (tau**b)/E, fN=3, fmt='g-',\
        xlabel='$\\tau$, sec.', ylabel='J, 1/Pa', logplot=True, newplot=False)

#wait for the user:
dummy=input("Press ENTER to continue")

print("Run the Mason conversion")
t = time()
G = J_to_G_Mason(J, advanced=False, verbose=True)
print("Done in %.2f seconds\n\n" %(time()-t))

#Theoretical value:
ipi = nu.complex(0.0, 1.0)*nu.pi
ca = nu.complex(1.0/E) #the complex amplitude of J
G['G2'] = nu.exp(ipi*0.5*b)*\
        G['omega'].astype(nu.complex)**b / (ca*gamma(1.0+b))

err = (nu.abs(G['G2'] - G['G'])**2 / nu.abs(G['G2'])).mean()
print("Fitting (rel)error: %.3g\n" %err)

Plot(G['omega'], G['G'].real, fN=2, fmt='r+',\
     logplot=True)
Plot(G['omega'], G['G'].imag, fN=2, fmt='b+',\
     logplot=True, newplot= False)
Plot(G['omega'], G['G2'].real, fN=2, fmt='k-',\
     logplot=True, newplot= False)
Plot(G['omega'], G['G2'].imag, fN=2, fmt='g-',\
     logplot=True, xlabel="$\omega$, 1/s", ylabel="G', G'', Pa",\
     newplot= False)

#done with testing the Mason method, wait for the user to play around:
dummy=input("Press ENTER to continue")

#Now test the Evans method:
#MSD for a Maxwell body:
eta = 0.2 # Pas viscosity
print("Maxwell body: E=%.2f Pa, eta=%.2f Pas" %(E,eta))
MSD['MSD'] = (1/E + tau/eta)/Const
J = MSD_to_J(MSD, MSD['tau'][10], MSD['tau'][-10], T, a, ND, verbose=True)

print("Creep compliance is calculated for Maxwell liquid MSD")
Plot(J['tau'], J['J'], fN=1, logplot=True)
Plot(tau, 1/E + tau/eta, fN=1, fmt='r-',\
     xlabel='$\\tau$, sec.', ylabel='J, 1/Pa', logplot=True, newplot=False)

print("Converting to G by Evans method")
t = time()
G = J_to_G(J, filter=True)
print("done in %.3f seconds" % (time()-t))

#the theoretical value:
icom = (G['omega']).astype(nu.complex)*nu.complex(0.0, 1.0)
G['G2'] = icom*E*eta/(eta*icom + E)

err = (nu.abs(G['G2'] - G['G'])**2 / nu.abs(G['G2'])).mean()
print("Fitting (rel)error: %.3g\n" % err)

#Display results:
Plot(G['omega'], G['G'].real, fN=2, fmt='r+',
     logplot=True)
Plot(G['omega'], G['G'].imag, fN=2, fmt='b+',
     logplot=True, newplot= False)
Plot(G['omega'], G['G2'].real, fN=2, fmt='k-',
     logplot=True, newplot= False)
Plot(G['omega'], G['G2'].imag, fN=2, fmt='g-',
     logplot=True, xlabel="$\omega$, 1/s", ylabel="G', G'', Pa",
     newplot= False)
#done
input("Press ENTER to continue")

print("\nCalculate a Kelvin-Voigt solid with E= %.2f Pa, eta= %.2f Pas" %(E,eta))
MSD['MSD'] = 1.0/(E*Const)*(1.0 - nu.exp(-E/eta * tau))
J = MSD_to_J(MSD, t0, tend, T, a, ND, verbose=True)

#Display:
print("Creep compliance is calculated for Kelvin-Voigt solid MSD")
Plot(J['tau'], J['J'], fN=1, logplot=True)
Plot(tau, 1/E *(1.0 - nu.exp(-E/eta*tau)), fN=1, fmt='r-',
     xlabel='$\\tau$, sec.', ylabel='J, 1/Pa', logplot=True, newplot=False)

#Now the G:
print("Converting to G by Evans method")
t = time()
G = J_to_G(J)
print("done in %.3f seconds" %(time()-t))
#the theoretical value:
icom = (G['omega']).astype(nu.complex)*nu.complex(0.0, 1.0)
G['G2'] = E + icom*eta

err = (nu.abs(G['G2'] - G['G'])**2 / nu.abs(G['G2'])).mean()
print("Fitting (rel)error: %.3g\n" % err)

#Display results:
Plot(G['omega'], G['G'].real, fN=2, fmt='r+',\
     logplot=True)
Plot(G['omega'], G['G'].imag, fN=2, fmt='b+',\
     logplot=True, newplot= False)
Plot(G['omega'], G['G2'].real, fN=2, fmt='k-',\
     logplot=True, newplot= False)
Plot(G['omega'], G['G2'].imag, fN=2, fmt='g-',\
     logplot=True, xlabel="$\omega$, 1/s", ylabel="G', G'', Pa",\
     newplot= False)
#done

print("\n\nThe match is quite not perfect at the high frequency range")
input("Press ENTER to continue")

#Improve things using the dynamic interpolation method:
print("Fitting the MSD")
print("the fit has to result in paraneters [%.4f, %.4f]" %(1/(E*Const), E/eta))
fits = MSD_power_fit(MSD, 100, fill=0.9, scaler=10, Kelvin=True, verbose=True)
print("Dynamic interpolation")
MSD2 = MSD_interpolate(MSD, fits, smoothing=False, Nsmoothing=10, 
                       insert=10,
                       #dynamic resampling:
                       factor=-1,
                       eps=5E-4,
                       verbose=True)

J2 = MSD_to_J(MSD2, MSD2['tau'][10], MSD2['tau'][-100], T, a, ND, verbose=True)
print("\nConverting to G by Evans method")
t = time()
G2 = J_to_G(J2, omax=G['omega'].max())
print("done in %.3f seconds" % (time()-t))

#the theoretical value:
icom = (G2['omega']).astype(nu.complex)*nu.complex(0.0, 1.0)
G2['G2'] = E + icom*eta

err = (nu.abs(G2['G2'] - G2['G'])**2 / nu.abs(G2['G2'])).mean()
print("Fitting (rel)error after resampling: %.3g\n" % err)

#Display results:
Plot(G2['omega'], G2['G'].real, fN=2, fmt='r+',
     logplot=True)
Plot(G2['omega'], G2['G'].imag, fN=2, fmt='b+',
     logplot=True, newplot=False)
Plot(G2['omega'], G2['G2'].real, fN=2, fmt='k-',
     logplot=True, newplot=False)
Plot(G2['omega'], G2['G2'].imag, fN=2, fmt='g-',
     logplot=True, xlabel=r"$\omega$, 1/s", ylabel="G', G'', Pa",
     newplot=False)
#done

dummy = input("Function-test done. Press ENTER to quit.")
